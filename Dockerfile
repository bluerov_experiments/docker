#Base docker image

FROM ubuntu:bionic

RUN apt-get -y update
RUN apt-get install -y software-properties-common
#RUN add-apt-repository ppa:deadsnakes/ppa -y
#RUN apt-get -y update


RUN apt-get -y install gawk git python-pip cmake 
RUN echo "export PS1=\\\\\\\\w\\$" >> /etc/bash.bashrc
RUN pip install --upgrade pip
RUN apt-get -y install build-essential
RUN apt-get -y install wget curl
#python packages
RUN apt-get -y install python-zmq
RUN apt-get -y install libevent1-dev libncurses5-dev
ENV LC_CTYPE=C.UTF-8
RUN apt-get -y install python3
RUN apt-get -y install python3-dev
RUN wget https://bootstrap.pypa.io/get-pip.py -O /tmp/get-pip.py
RUN python3 /tmp/get-pip.py
RUN pip3 install numpy
RUN pip3 install --upgrade pip
RUN pip3 install pyzmq



######## ros kinetic ######
RUN echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list
RUN apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116


RUN apt-get -y update
RUN apt-get -y install ros-melodic-desktop-full
RUN apt-get -y install python-wstool python-rosinstall-generator python-catkin-tools
RUN apt-get -y install ros-melodic-mavros ros-melodic-mavros-extras
RUN /opt/ros/kinetic/lib/mavros/install_geographiclib_datasets.sh


#additional ros libs
RUN apt-get -y install ros-melodic-image-pipeline
RUN apt-get install -y sudo
RUN apt-get install -y libprotobuf-dev

###python3 ros support
RUN pip3 install pyyaml rospkg catkin_pkg


###added more ros libs
RUN apt-get install -y python-rosinstall

### add python dynamics
RUN apt-get install -y  python-testresources
RUN pip2 install pydy
RUN pip2 install future

RUN cd /; git clone --depth 1 http://bitbucket.org/bluerov_experiments/mavlink; cd mavlink; git submodule update --init
RUN cd /mavlink/pymavlink; sudo MDEF=/mavlink/message_definitions  pip install .
RUN cd /; git clone --depth 1 http://bitbucket.org/bluerov_experiments/dronekit-python; 
RUN cd /dronekit-python; sudo pip install .
RUN mkdir /catkin
RUN sudo rosdep init; sudo rosdep update
RUN bash -c "source /opt/ros/melodic/setup.bash; cd /catkin; mkdir src; catkin_make; cd src; git clone https://github.com/patrickelectric/bluerov_ros_playground; git clone--depth 1  https://github.com/freefloating-gazebo/freefloating_gazebo; git clone --depth 1 https://github.com/freefloating-gazebo/freefloating_gazebo_demo; cd /catkin; rosdep install --from-paths src --ignore-src -r -y"
RUN bash -c "source /opt/ros/melodic/setup.bash; cd /catkin; catkin_make"
RUN apt-get install -y net-tools
RUN apt-get install -y iputils-ping

# FAKE X
# Install vnc, xvfb in order to create a 'fake' display and firefox
RUN     apt-get install -y x11vnc xvfb
RUN     mkdir ~/.vnc
# Setup a password
RUN     x11vnc -storepasswd 1234 ~/.vnc/passwd
RUN apt-get install -y screen nano

RUN pip install mavproxy
COPY host_entry.sh /host_entry.sh 
RUN mkdir /Xauthority    
RUN chmod +x /host_entry.sh   


