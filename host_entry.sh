#!/bin/bash

if [ -z "$XFB_SCREEN" ]; then
	XFB_SCREEN=1024x768x24
fi

if [ ! -z "$XFB_SCREEN_DPI" ]; then
	DPI_OPTIONS="-dpi $XFB_SCREEN_DPI"
fi

mcookie | sed -e 's/^/add :0 MIT-MAGIC-COOKIE-1 /' | xauth -q
xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f /Xauthority/xserver.xauth nmerge -
Xvfb :0 -auth ~/.Xauthority $DPI_OPTIONS -screen 0 $XFB_SCREEN >>~/xvfb.log 2>&1 &
sleep 2

 screen -dmS gazebo bash -c "export DISPLAY=:0; cd /catkin; source devel/setup.bash; export ROS_HOSTNAME=localhost; roslaunch bluerov_ros_playground gazebo.launch"
 screen -dmS sitl bash -c "export DISPLAY=:0; export PATH=$PATH:/ardupilot/Tools/autotest; cd /ardupilot/ArduSub; sim_vehicle.py --console"
